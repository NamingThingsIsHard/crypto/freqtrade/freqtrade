# Checklist

<!-- If you don't tick off everything here, your request will be closed without further comment -->

 - [ ] I have checked the [documentation] to see if this has already been implemented
 - [ ] I have searched for similar tickets

## Describe your environment
(if applicable)

  * Operating system: ____
  * Python Version: _____ (`python -V`)
  * CCXT version: _____ (`pip freeze | grep ccxt`)
  * Freqtrade Version: ____ (`freqtrade -V` or `docker-compose run --rm freqtrade -V` for Freqtrade running in docker)


# Describe the enhancement


# How could it be implemented?

<!-- Not necessary, but if you've already looked into it or have a concrete plan, please describe it -->

[documentation]: https://namingthingsishard.gitlab.io/crypto/freqtrade/freqtrade/