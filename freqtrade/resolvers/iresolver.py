# pragma pylint: disable=attribute-defined-outside-init

"""
This module load custom objects
"""
import importlib.util
import inspect
import logging
import os
import sys
from pathlib import Path
from types import ModuleType
from typing import Any, Dict, Iterator, List, Optional, Tuple, Type, Union

from freqtrade.exceptions import OperationalException


logger = logging.getLogger(__name__)


class IBaseResolver:
    """
    This class contains all the logic to load custom classes
    """
    # Childclasses need to override this
    object_type: Type[Any]
    object_type_str: str
    user_subdir: Optional[str] = None
    initial_search_path: Optional[Path]

    @classmethod
    def build_search_paths(cls, config: Dict[str, Any], user_subdir: Optional[str] = None,
                           extra_dir: Optional[str] = None) -> List[Path]:

        abs_paths: List[Path] = []
        if cls.initial_search_path:
            abs_paths.append(cls.initial_search_path)

        if user_subdir:
            abs_paths.insert(0, config['user_data_dir'].joinpath(user_subdir))

        if extra_dir:
            # Add extra directory to the top of the search paths
            abs_paths.insert(0, Path(extra_dir).resolve())

        return abs_paths

    @classmethod
    def _get_valid_object(cls, module_path: Path, object_name: Optional[str],
                          enum_failed: bool = False) -> Iterator[Any]:
        """
        Generator returning objects with matching object_type and object_name in the path given.
        :param module_path: absolute path to the module
        :param object_name: Class name of the object
        :param enum_failed: If True, will return None for modules which fail.
            Otherwise, failing modules are skipped.
        :return: generator containing tuple of matching objects
             Tuple format: [Object, source]
        """

        # Generate spec based on absolute path
        # Pass object_name as first argument to have logging print a reasonable name.
        spec = importlib.util.spec_from_file_location(object_name or "", str(module_path))
        if not spec:
            return iter([None])

        module = importlib.util.module_from_spec(spec)
        try:
            spec.loader.exec_module(module)  # type: ignore # importlib does not use typehints
        except (ModuleNotFoundError, SyntaxError, ImportError, NameError) as err:
            # Catch errors in case a specific module is not installed
            logger.warning(f"Could not import {module_path} due to '{err}'")
            if enum_failed:
                return iter([None])

        valid_objects_gen = (
            (obj, inspect.getsource(module)) for
            name, obj in inspect.getmembers(
                module, inspect.isclass) if ((object_name is None or object_name == name)
                                             and issubclass(obj, cls.object_type)
                                             and obj is not cls.object_type)
        )
        return valid_objects_gen

    @classmethod
    def search_object(
            cls, directory: Path, *, object_name: str, add_source: bool = False
    ) -> Union[Tuple[Any, Path], Tuple[None, None]]:
        """
        Search for the objectname in the given directory
        :param directory: relative or absolute directory path
        :param object_name: ClassName of the object to load
        :return: object class
        """
        logger.debug(f"Searching for {cls.object_type.__name__} {object_name} in '{directory}'")
        for entry in directory.iterdir():
            # Only consider python files
            if entry.suffix != '.py':
                logger.debug('Ignoring %s', entry)
                continue
            if entry.is_symlink() and not entry.is_file():
                logger.debug('Ignoring broken symlink %s', entry)
                continue
            module_path = entry.resolve()

            obj = next(cls._get_valid_object(module_path, object_name), None)

            if obj:
                obj[0].__file__ = str(entry)
                if add_source:
                    obj[0].__source__ = obj[1]
                return (obj[0], module_path)
        return (None, None)

    @classmethod
    def _load_object(cls, paths: List[Path], *, object_name: str, add_source: bool = False,
                     kwargs: dict = {}) -> Optional[Any]:
        """
        Try to load object from path list.
        """

        for _path in paths:
            try:
                (module, module_path) = cls.search_object(directory=_path,
                                                          object_name=object_name,
                                                          add_source=add_source)
                if module:
                    logger.info(
                        f"Using resolved {cls.object_type.__name__.lower()[1:]} {object_name} "
                        f"from '{module_path}'...")
                    return module(**kwargs)
            except FileNotFoundError:
                logger.warning('Path "%s" does not exist.', _path.resolve())

        return None

    @classmethod
    def load_object(cls, object_name: str, config: dict, *, kwargs: dict,
                    extra_dir: Optional[str] = None) -> Any:
        """
        Search and loads the specified object as configured in hte child class.
        :param object_name: name of the module to import
        :param config: configuration dictionary
        :param extra_dir: additional directory to search for the given pairlist
        :raises: OperationalException if the class is invalid or does not exist.
        :return: Object instance or None
        """

        abs_paths = cls.build_search_paths(config,
                                           user_subdir=cls.user_subdir,
                                           extra_dir=extra_dir)

        found_object = cls._load_object(paths=abs_paths, object_name=object_name,
                                        kwargs=kwargs)
        if found_object:
            return found_object
        raise OperationalException(
            f"Impossible to load {cls.object_type_str} '{object_name}'. This class does not exist "
            "or contains Python code errors."
        )


class IResolver(IBaseResolver):
    """
    Interface to allow loading python objects using python's import mechanics
    """

    @classmethod
    def search_object(cls, directory: Path, *, object_name: str, add_source: bool = False
                      ) -> Union[Tuple[Any, Path], Tuple[None, None]]:
        """
        Import object from the given directory.
        Falls back to a simple python file search.

        ..see:: `IResolver.import_object`_

        :param directory: relative or absolute directory path
        :param object_name: ClassName or dot separated import path
        :return: object class
        """
        logger.debug(f"Attempting import of {cls.object_type.__name__} {object_name} "
                     f"from '{directory}'")
        try:
            item, module = cls.import_object(object_name, directory)
            setattr(item, "__file__", module.__file__)
            module_path = Path(module.__file__)
            if add_source:
                setattr(item, "__source__", module_path.read_text())
            return item, module_path
        except (ImportError, ModuleNotFoundError) as exc:
            logger.info("Falling back to old resolution method")
            try:
                super_result = super().search_object(
                    directory,
                    object_name=object_name,
                    add_source=add_source,
                )
                if not super_result or not all(super_result):
                    logger.exception(
                        "Couldn't resolve object %s with python import nor with old resolver",
                        object_name
                    )
                return super_result
            except Exception as super_exc:
                raise super_exc from exc

    @classmethod
    def search_all_objects(cls, directory: Path,
                           enum_failed: bool) -> List[Dict[str, Any]]:
        """
        Searches a directory for valid objects

        :param directory: Path to search
        :param enum_failed: If True, will return None for modules which fail.
            Otherwise, failing modules are skipped.
        :return: List of dicts containing 'name', 'class' and 'location' entries
        """
        logger.debug(f"Searching for {cls.object_type.__name__} '{directory}'")
        # Make sure we can import modules and packages from the directory
        cls._insert_sys_path(directory)

        objects = []
        for module_dot_path, module_path in cls.find_modpacks(directory):
            valid_objects: List[Tuple[Optional[str], Any]] = []
            module = None
            try:
                module = cls.import_module(module_dot_path)
                valid_objects = [
                    (obj_name, obj) for
                    obj_name, obj in inspect.getmembers(module, inspect.isclass)
                    if (issubclass(obj, cls.object_type) and obj is not cls.object_type)
                ]
            except ImportError as import_error:
                logger.debug("Couldn't import from %s: %s", module_dot_path, import_error)
                if enum_failed:
                    valid_objects = [(None, None)]

            for obj_name, obj in valid_objects:
                if obj and module and not getattr(obj, "__source__", None):
                    setattr(obj, "__source__", inspect.getsource(module))
                objects.append(
                    {'name': cls.build_import_path(obj_name, module) or "",
                     'class': obj,
                     'location': module_path,
                     })

        return objects

    @classmethod
    def find_modpacks(cls, where: Path) -> List[Tuple[str, Path]]:
        """
        Find all modules and packages in the given path

        :return: the module or pack name + the path to it
        """
        modpacks = []
        for root, dirs, files in os.walk(where, followlinks=True):
            # Copy dirs to iterate over it, then empty dirs.
            all_dirs = dirs[:]
            # Modifying dirs controls where os.walk will enter
            dirs[:] = []

            # Find packages
            for item in all_dirs:
                full_path = os.path.join(root, item)
                rel_path = os.path.relpath(full_path, where)
                modpack = rel_path.replace(os.path.sep, '.')

                # Skip items (and directories) that are not valid packages
                if not cls._looks_like_package(full_path):
                    continue
                modpacks.append((modpack, Path(full_path) / "__init__.py"))

                if item in all_dirs:
                    dirs.append(item)

            # Find modules
            for item in (all_dirs + files):
                if item == "__init__.py":
                    continue
                full_path = os.path.join(root, item)

                # Skip items (and directories) that are not valid packages
                module_name = inspect.getmodulename(full_path)
                if not module_name:
                    continue

                full_mod_path = os.path.join(root, module_name)
                rel_path = os.path.relpath(full_mod_path, where)
                module_path = rel_path.replace(os.path.sep, '.')
                modpacks.append((module_path, Path(full_path)))

        # Sort by depth in order to import parent modules first
        return sorted(modpacks, key=lambda item: item.count("."))

    @classmethod
    def build_import_path(cls, obj_name: str = None, module: ModuleType = None) -> Optional[str]:
        """
        Create an import path that can be used to import an object using this resolver

        :param obj_name: What the object is called within the module
        :param module: The module from whence the object comes

        :return: A possibly dot separated import path to use with `import_object`_
        """
        if not (obj_name and module):
            return None
        mod_name = module.__name__
        # If the module is in the root, then we will be able to fallback to just the name
        if obj_name == mod_name:
            return obj_name
        else:
            return f"{mod_name}.{obj_name}"

    @staticmethod
    def _looks_like_package(path: str):
        """Does a path look like a package?"""
        return "." not in path and os.path.isfile(os.path.join(path, '__init__.py'))

    @classmethod
    def import_object(cls, name: str, path: Path) -> Tuple[type, ModuleType]:
        """
        Tries to import an item from a module at a given path

        The path is put into sys.path in order for things like unpickling to work later on

        ..Examples::

            import_object("MyClass") --> from MyClass import MyClass
            import_object("path.to.MyClass") --> from path.to import MyClass

        Two approaches will be attempted with two variants

          For a simple name (no dots) the equivalent of `from <name> import <name>`:

             - <name>.py containing `class <name>`
             - <name>/__init.py containing `class <name>`

          For a dot-separated name the equivalent of
         `from <name before last dot> import <name after last dot>`

             - <name1>/<name2>/<nameX>.py containing `class <name>
             - <name1/<name2>/<nameX>/__init.py containing `class <name>`

        :param name: A valid python module and import path or name
        :param path: Where to search the python object
        :return: The item to import
        :throws: ImportError
        """
        abs_path = path.resolve(strict=True)
        cls._insert_sys_path(abs_path)
        module = cls.import_module(name, import_from=True)
        try:
            item = getattr(module, name.rpartition(".")[2])
        except AttributeError:
            raise ImportError(name=name, path=str(abs_path))

        return item, module

    @classmethod
    def _insert_sys_path(cls, path: Path):
        # Make sure our path is the first one to be searched in order to remove ambiguities
        str_path = str(path)
        index: Optional[int] = None
        try:
            index = sys.path.index(str_path)
        except ValueError:
            pass
        if index is not None and index != 0:
            sys.path.remove(str_path)
        sys.path.insert(0, str_path)

    @staticmethod
    def import_module(name: str, import_from=False) -> ModuleType:
        """
        Interprets the given string as a path to import a module

        :param: name: the simple name of a module, or a path to a module or class within a module
                "some_module" --> import some_module
                "path.to.some_module.SomeClass" --> from path.to.some_module import Someclass
        : param: import_from: indicates that the last element in `name`
                    is to be imported from the module
        :returns: A module or throws an ImportError.
                   ..see: https://docs.python.org/3/library/functions.html#__import__
        """
        import_path, _, from_item = name.rpartition(".")
        kwargs: Dict[str, Any] = {}
        if import_path:
            if import_from:
                kwargs["name"] = import_path
            else:
                # When importing a deep modpack e.g import package.subpackage.module_or_package
                # __import__ requires the entire name and I can't be bothered to dig into why
                kwargs["name"] = name
            kwargs["fromlist"] = [from_item]
        else:
            kwargs["name"] = from_item
        kwargs.update(
            {
                "globals": globals(),
                "locals": locals(),
            }
        )
        # We need to reload here as we are dynamically importing and want to ignore the module cache
        return importlib.reload(importlib.__import__(**kwargs))
