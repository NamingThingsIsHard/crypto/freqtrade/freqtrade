#!/usr/bin/env python3
import argparse
import dataclasses
import datetime
import json
import logging
import os
import re
import subprocess
import typing
from pathlib import Path
from typing import List, Union
from unittest import mock

import freqtrade.rpc.api_server.api_v1
from freqtrade.rpc.api_server import ApiServer


THIS = Path(__file__).absolute()
THIS_DIR = THIS.parent
OPENAPI_DIR = THIS_DIR.parent / ".openapi"
OPENAPI_JSON = OPENAPI_DIR / "openapi.json"
OPENAPI_CLIENTS_DIR = OPENAPI_DIR / "clients"
OPENAPI_GEN_CONF_PATH = THIS_DIR / "openapi_generator.yml"
OPENAPI_DOCKER_IMAGE = "openapitools/openapi-generator-cli:v5.1.1"


@dataclasses.dataclass
class LangConf:
    repo: str
    params: typing.Dict[str, typing.Union[str, bool]]


@dataclasses.dataclass
class GeneratorConf:
    langs: typing.Dict[str, LangConf]


def main(indent: int, should_push_client: bool):
    # Create the openapi dir
    OPENAPI_DIR.mkdir(parents=True, exist_ok=True)

    write_openapi_json(indent)
    base_repo = os.environ.get(
        "CLIENTS_BASE_REPO",
        "git@gitlab.com:NamingThingsIsHard/crypto/freqtrade/clients"
    )
    generator_conf = GeneratorConf(langs={
        "javascript": LangConf(
            repo=f"{base_repo}/freqtrade-client-js.git",
            params=dict(
                apiPackage="ftclient",
                projectName="freqtrade-client-js",
                moduleName="ftclient",
                projectDescription="Client library for freqtrade",
                usePromises="true",
            )
        ),
        "python": LangConf(
            repo=f"{base_repo}/freqtrade-client-py.git",
            params=dict(
                packageName="ftclient",
                projectName="freqtrade-client-py"
            )
        )
    })

    for lang, lang_conf in generator_conf.langs.items():
        logging.info("Generating client for: %s", lang)
        repo_path = OPENAPI_CLIENTS_DIR / lang

        if not repo_path.exists():
            clone_client(repo_path, lang_conf.repo)
        generate_client(repo_path, lang, lang_conf)
        if should_push_client:
            push_client(repo_path)
    logging.info("Generated all clients")


def write_openapi_json(indent):
    with mock.patch.object(ApiServer, "start_api"):
        openapi_dict = get_openapi_dict()

        # Set version to the freqtrade version
        version = freqtrade.__version__
        if not is_version_string_ok(version):
            utc_date = datetime.datetime.utcnow()
            version = f"{utc_date.year}.{utc_date.month}-dev{utc_date.day}"
        openapi_dict["info"]["version"] = version
        with open(OPENAPI_JSON, "w") as json_file:
            json.dump(openapi_dict, json_file, indent=indent)


def is_version_string_ok(version: str) -> bool:
    """
    Checks if version string conforms to PEP440

    Code from https://www.python.org/dev/peps/pep-0440
    """
    return re.match(
        r'^([1-9][0-9]*!)?'
        r'(0|[1-9][0-9]*)'
        r'(\.(0|[1-9][0-9]*))*'
        r'((a|b|rc)(0|[1-9][0-9]*))?'
        r'(\.post(0|[1-9][0-9]*))?'
        r'(\.dev(0|[1-9][0-9]*))?$',
        version) is not None


def get_openapi_dict():
    server = ApiServer({"api_server": {}})
    openapi_dict = server.app.openapi()
    return openapi_dict


def get_generator_conf() -> dict:
    ...


def clone_client(path: Path, repo_url: str):
    subprocess.check_call(["git", "clone", repo_url, str(path)])


def generate_client(path: Path, lang: str, lang_conf: LangConf):
    gen_args: List[Union[str, Path]] = [
        "openapi-generator-cli",
        "generate", "-i", OPENAPI_JSON,
        "-g", lang,
        "-o", path,
    ]
    for name, value in lang_conf.params.items():
        gen_args.append("-p")
        gen_args.append(f"{name}={value}")
    subprocess.check_call(gen_args)


def push_client(path: Path):
    subprocess.check_call(["git", "add", "."], cwd=path)

    # Make sure something changed otherwise git commit will be unhappy
    status_res = subprocess.run("git status -s".split(" "), stdout=subprocess.PIPE, cwd=path)
    if len(status_res.stdout) == 0:
        logging.info("Nothing to commit in %s", path)
        return
    subprocess.check_call(["git", "commit", "--all", "-m", "Minor update"], cwd=path)
    subprocess.check_call(["git", "push"], cwd=path)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(description="Simply outputs the OpenAPI JSON of the REST API")
    parser.add_argument("-i", "--indent", type=int, default=2)
    parser.add_argument("-p", "--push", action="store_true",
                        help="Push the generated clients to their repos")

    args = parser.parse_args()

    main(args.indent, args.push)
